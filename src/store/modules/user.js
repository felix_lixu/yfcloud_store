export default {
  namespaced: true,
  state: {
    id: 0,
    name: '',
    shopId: ''
  },
  mutations: {
    updateId(state, id) {
      state.id = id
    },
    updateName(state, name) {
      state.name = name
    },
    updateShopId(state, shopId) {
      state.shopId = shopId
    }
  }
}
